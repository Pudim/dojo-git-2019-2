package br.ucsal.bes20192.gcm.aplicativos;

public class CalculadoraUtil {

	// A soma esta muito certa para a mente do usu�rio
	public static Integer somar(Integer numero1, Integer numero2) {
		return numero1 + numero2;
	}
	
	public static Integer subtrair(Integer numero1, Integer numero2) {
		return numero1 - numero2;
	}
	
	public static Integer multiplicar(Integer numero1, Integer numero2) {
		return numero1 * numero2;
	}
	
	public static Integer dividir(Integer numero1, Integer numero2) {
		return numero1 / numero2;
	}
}

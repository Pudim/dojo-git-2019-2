package br.ucsal.bes20192.gcm.aplicativos.teste;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import br.ucsal.bes20192.gcm.aplicativos.CalculadoraUtil;

public class CalculadoraUtilTest {

	@Test
	public void testarSoma(){
		Integer resultado = CalculadoraUtil.somar(5, 12);
		Integer resultadoe = 17;
		assertEquals(resultadoe, resultado);
		
	}
	
	@Test
	public void testarSubtracao(){
		Integer resultado = CalculadoraUtil.subtrair(12, 5);
		Integer resultadoe = 7;
		assertEquals(resultadoe, resultado);
		
	}

	@Test
	public void testarMultiplicacao(){
		Integer resultado = CalculadoraUtil.multiplicar(5, 5);
		Integer resultadoe = 25;
		assertEquals(resultadoe, resultado);
		
	}
	
	@Test
	public void testarDivisao(){
		Integer resultado = CalculadoraUtil.dividir(5, 5);
		Integer resultadoe = 1;
		assertEquals(resultadoe, resultado);
		
	}

}
